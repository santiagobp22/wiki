## Problem selected and proposed solution

**Made by all members of the group.**

The general **problem** we selected is that the use of WhatsApp groups to make use of wheels, both as passengers and as drivers, is inconvenient, and has many improvement points: tedious search for drivers and passengers, cancellations, not well-defined routes, size-limit of members of WhatsApp groups, the fact that WhatsApp groups are not centralized, the spam that comes from WhatsApp groups, and the cash inconveniences when paying the trips.

Our general **solution** as a software development team is to implement an app that tackles each one of those problems. This app will allow the user to automatically be matched with their counterpart if wished; thus, solving the tedious search and the last-minute cancellations. Nonetheless, the app will also allow users to search manually for their counterparts. Besides, the app will implement a rating system similar to that of Uber, Didi, etc, in which both passengers and drivers can rate their counterparts when utilizing the service. This rating will impact the search time for both types of users when automatically searching (future research will be made on which cancellation politics comply better with both penalized and affected users); thus, helping solve the cancellations problem. Drivers will be able to select one or more specific wished routes; thus, clarifying the not well-defined routes. Finally, thanks to the fact that all the process will be made through an app, this app won't have the problems inherent to WhatsApp groups like spam and limited members and will allow the user to make payments in other than cash.

## Analytic Persona

**Made by all members of the group.**

![](Multimedia/Images/Analytics_persona.jpg)

## Problem-Alternative-Solutions

**Made by all members of the group.**

| Problem | Alternatives | Solutions |
| --- | --- | --- |
| Searching for passengers or drivers | Making use of WhatsApp groups | We are better because we automate the process for them |
| Last minute cancellations | Searching quicly in WhatsApp groups other persons | We are better because we assure users that we will find a passenger or drivers if there are available |
| There are not enough passengers or drivers | Joining more than one WhatsApp group | By specifying the route, passengers will be matched with drivers from different areas that happen to go through that area | 
| Drivers and passengers can't assure the reliability of their counterpart | Going through the experience themselves | We will know by the experience of others whether or not people are reliable, and we will be able to warn them with time |
| There is not enough room in some WhatsApp groups | Creating new WhatsApp groups | We won't have that room limit problem by the nature of our app |
| There is not enough room in some WhatsApp groups | Waiting for a place to free to join the group | We won't have that room limit problem by the nature of our app |
| Spam of WhatsApp groups makes them lose opportunities when muting the groups | Manually reviewing the messages of the group | Users won't have to review all messages of a chat group, only the current availiable trips |
| There is not always cash to pay | Users agree to see themselves later in the day | We will allow for the possibility of using credits or e-platforms to pay |
| Drivers can't complain about the users | Drivers decide by their experience which passengers they are offering the service | We offer a service of ratings on the app so that all drivers could rate the passengers on their trip|
| There isn't a standard fee among the trips on wheels | Drivers adjust the price by seeing what are the prices that the other drivers offer on the wahtsapp groups| We would guarantee a standard fee based on the nature of the travels |
| Passengers lose time searching the travels they need among the 100 or more messages on the whatsapp groups | Users try to contact the same driver they met on previous travels | All the travels must be organized on our app, for all passengers so that they can have easy access to this information |
| The under commitment from passengers and drivers | Users accept the way wheels works, but expect this could imporve | We offer a rating system dedicated to both passengers and drivers so the app will provide a environment where all are commited to their travels |
 

 ## Context Canvas
 **Made by all members of the group.**

 ![](Multimedia/Images/CONTEXT_CANVAS_WHEELS.png)


 ## Business questions

**Made by all members of the group.**

**Type 1:**
* How many times a seat is assigned to more than one person? (Given the concurrency problem).

**Type 2:**
* Do users prefer paying in credits, cash or credit card? (To suggest them constantly their default).
* Does Waze consider that traffic is good on that route the driver selected?
* Is the user asking for the service a bad user? (So to warn the other users).
* Have there been an increased insecurity outbreak in that route?

**Type 3:**
* Are drivers really using the "maximum number of people" feature?
* Are users using the manual search feature of the app?
* Are users using the automatic search feature of the app?

**Type 4:**
* What incidents happened in which areas? (We could sell that information to the government).

**Type 5:**
* Which is the preferred departure and arrival time for students? (May give importan insight to Uniandes and it's class planning, and also allows us to recommend the user a service ahead of time).
 ## VD-Map / Wheels
 **Made by all members of the group.**

 
![](Multimedia/Images/VD-map-final.png)

